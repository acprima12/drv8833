#include<SPI.h>
#define a1 6
#define a2 7
#define b1 8
#define b2 9

#define stb1 0
#define stb2 1
#define stb3 2
#define stb4 3
#define stb5 4
#define stb6 5                 // stb1 kuning; stb2 orange; stb3 biru;  // stb4 biru; stb5 ungu; stb6 hijau
#define dout 7
//SCLK = 13; din/MOSI = 11;

void setup() {
    digitalWrite(SS, HIGH);
    SPI.begin();
    SPI.setClockDivider(SPI_CLOCK_DIV8);
    SPI.setDataMode(SPI_MODE0);

    pinMode(a1, OUTPUT);
    pinMode(a2, OUTPUT);
    pinMode(b1, OUTPUT);
    pinMode(b2, OUTPUT);
    pinMode(stb1, OUTPUT);
    pinMode(stb2, OUTPUT);
    pinMode(stb3, OUTPUT);
    pinMode(stb4, OUTPUT);
    pinMode(stb5, OUTPUT);
    pinMode(stb6, OUTPUT);  
}
void loop() {
    
    //printingline();
    paperin();
//    paperout();
  
}

void printingline(){
    digitalWrite(SS, LOW);
    delayMicroseconds(10);
    SPI.transfer(0xAA);
    digitalWrite(stb1, HIGH);
    digitalWrite(stb2, HIGH);
    digitalWrite(stb3, HIGH);
    digitalWrite(stb4, HIGH);
    digitalWrite(stb5, HIGH);
    digitalWrite(stb6, HIGH);
    digitalWrite(SS, HIGH);  
}
void paperin()
{  

         //step4
    digitalWrite(a1, LOW);
    digitalWrite(a2, HIGH);
    digitalWrite(b1, LOW);
    digitalWrite(b2, HIGH);
    delay(5); 


    //step3
    digitalWrite(a1, LOW);
    digitalWrite(a2, HIGH);
    digitalWrite(b1, HIGH);
    digitalWrite(b2, LOW);
    delay(5);
    
 

      //step2
    digitalWrite(a1, HIGH);
    digitalWrite(a2, LOW);
    digitalWrite(b1, HIGH);
    digitalWrite(b2, LOW);
    delay(5);
    


          //step1
    digitalWrite(a1, HIGH);
    digitalWrite(a2, LOW);
    digitalWrite(b1, LOW);
    digitalWrite(b2, HIGH);
    delay(5);   

    


}

void paperout(){
      //step1
    digitalWrite(a1, HIGH);
    digitalWrite(a2, LOW);
    digitalWrite(b1, LOW);
    digitalWrite(b2, HIGH);
    delay(1);

    //step2
    digitalWrite(a1, HIGH);
    digitalWrite(a2, LOW);
    digitalWrite(b1, HIGH);
    digitalWrite(b2, LOW);
    delay(1);

    //step3
    digitalWrite(a1, LOW);
    digitalWrite(a2, HIGH);
    digitalWrite(b1, HIGH);
    digitalWrite(b2, LOW);
    delay(1);

    //step4
    digitalWrite(a1, LOW);
    digitalWrite(a2, HIGH);
    digitalWrite(b1, LOW);
    digitalWrite(b2, HIGH);
    delay(1); 
 }
